﻿namespace clock1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.setTimerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minutesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.minutesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.hourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minutesToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.minutesToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.minutesToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.minutesToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pauseplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Digital-7", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 49);
            this.label1.TabIndex = 0;
            this.label1.Text = "Timer";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.SizeChanged += new System.EventHandler(this.label1_SizeChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setTimerToolStripMenuItem,
            this.toggleToolStripMenuItem,
            this.minToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.pauseplayToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(133, 114);
            // 
            // setTimerToolStripMenuItem
            // 
            this.setTimerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.minutesToolStripMenuItem1,
            this.minutesToolStripMenuItem2,
            this.hourToolStripMenuItem,
            this.minutesToolStripMenuItem3,
            this.minutesToolStripMenuItem4,
            this.minutesToolStripMenuItem5,
            this.minutesToolStripMenuItem6});
            this.setTimerToolStripMenuItem.Name = "setTimerToolStripMenuItem";
            this.setTimerToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.setTimerToolStripMenuItem.Text = "set timer";
            // 
            // minutesToolStripMenuItem1
            // 
            this.minutesToolStripMenuItem1.Name = "minutesToolStripMenuItem1";
            this.minutesToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.minutesToolStripMenuItem1.Text = "52 | 17 minutes";
            this.minutesToolStripMenuItem1.Click += new System.EventHandler(this.minutesToolStripMenuItem1_Click);
            // 
            // minutesToolStripMenuItem2
            // 
            this.minutesToolStripMenuItem2.Name = "minutesToolStripMenuItem2";
            this.minutesToolStripMenuItem2.Size = new System.Drawing.Size(153, 22);
            this.minutesToolStripMenuItem2.Text = "26 | 8 minutes";
            this.minutesToolStripMenuItem2.Click += new System.EventHandler(this.minutesToolStripMenuItem2_Click);
            // 
            // hourToolStripMenuItem
            // 
            this.hourToolStripMenuItem.Name = "hourToolStripMenuItem";
            this.hourToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.hourToolStripMenuItem.Text = "+1 hour";
            this.hourToolStripMenuItem.Click += new System.EventHandler(this.hourToolStripMenuItem_Click);
            // 
            // minutesToolStripMenuItem3
            // 
            this.minutesToolStripMenuItem3.Name = "minutesToolStripMenuItem3";
            this.minutesToolStripMenuItem3.Size = new System.Drawing.Size(153, 22);
            this.minutesToolStripMenuItem3.Text = "+30 minutes";
            this.minutesToolStripMenuItem3.Click += new System.EventHandler(this.minutesToolStripMenuItem3_Click);
            // 
            // minutesToolStripMenuItem4
            // 
            this.minutesToolStripMenuItem4.Name = "minutesToolStripMenuItem4";
            this.minutesToolStripMenuItem4.Size = new System.Drawing.Size(153, 22);
            this.minutesToolStripMenuItem4.Text = "+15 minutes";
            this.minutesToolStripMenuItem4.Click += new System.EventHandler(this.minutesToolStripMenuItem4_Click);
            // 
            // minutesToolStripMenuItem5
            // 
            this.minutesToolStripMenuItem5.Name = "minutesToolStripMenuItem5";
            this.minutesToolStripMenuItem5.Size = new System.Drawing.Size(153, 22);
            this.minutesToolStripMenuItem5.Text = "+10 minutes";
            this.minutesToolStripMenuItem5.Click += new System.EventHandler(this.minutesToolStripMenuItem5_Click);
            // 
            // minutesToolStripMenuItem6
            // 
            this.minutesToolStripMenuItem6.Name = "minutesToolStripMenuItem6";
            this.minutesToolStripMenuItem6.Size = new System.Drawing.Size(153, 22);
            this.minutesToolStripMenuItem6.Text = "+5 minutes";
            this.minutesToolStripMenuItem6.Click += new System.EventHandler(this.minutesToolStripMenuItem6_Click);
            // 
            // toggleToolStripMenuItem
            // 
            this.toggleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.windowToolStripMenuItem,
            this.stickToolStripMenuItem});
            this.toggleToolStripMenuItem.Name = "toggleToolStripMenuItem";
            this.toggleToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.toggleToolStripMenuItem.Text = "toggle";
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.windowToolStripMenuItem.Text = "window";
            this.windowToolStripMenuItem.Click += new System.EventHandler(this.windowToolStripMenuItem_Click);
            // 
            // stickToolStripMenuItem
            // 
            this.stickToolStripMenuItem.Name = "stickToolStripMenuItem";
            this.stickToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.stickToolStripMenuItem.Text = "stick";
            this.stickToolStripMenuItem.Click += new System.EventHandler(this.stickToolStripMenuItem_Click);
            // 
            // minToolStripMenuItem
            // 
            this.minToolStripMenuItem.Name = "minToolStripMenuItem";
            this.minToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.minToolStripMenuItem.Text = "10 seconds";
            this.minToolStripMenuItem.Click += new System.EventHandler(this.secondsToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.closeToolStripMenuItem.Text = "close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // pauseplayToolStripMenuItem
            // 
            this.pauseplayToolStripMenuItem.Name = "pauseplayToolStripMenuItem";
            this.pauseplayToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.pauseplayToolStripMenuItem.Text = "pause/play";
            this.pauseplayToolStripMenuItem.Click += new System.EventHandler(this.pauseplayToolStripMenuItem_Click_1);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(413, 49);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Opacity = 0.7D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Timer";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem minToolStripMenuItem;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setTimerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minutesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem minutesToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem hourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minutesToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem minutesToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem minutesToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem minutesToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toggleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stickToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pauseplayToolStripMenuItem;
    }
}

