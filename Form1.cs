﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clock1
{
    public partial class Form1 : Form
    {
        int fiftytwo = 0;
        int areea;
        DateTime timertime;
        DateTime time2;
        public Form1()
        {
            InitializeComponent();
            Rectangle r = Screen.PrimaryScreen.WorkingArea;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, Screen.PrimaryScreen.WorkingArea.Height - this.Height);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToString();
            timer1.Start();
        }

        private void secondsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fiftytwo = 0;
            timertime = DateTime.Now.AddSeconds(10);
            timer2.Start();

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.CompareTo(timertime) == -1)
            {
                timer1.Stop();
                time2 = new DateTime(timertime.Subtract(DateTime.Now).Ticks);
                label1.Text = time2.ToString("HH:mm:ss");
            }
            else if (fiftytwo == 1 || fiftytwo == 2)
            {
                Console.Beep(3333, 130);
                Console.Beep(3333, 130);
                Console.Beep(3333, 390);
                minutesToolStripMenuItem1_Click(sender, e);
            }
            else if (fiftytwo == 3 || fiftytwo == 4)
            {
                Console.Beep(3333, 130);
                Console.Beep(3333, 130);
                Console.Beep(3333, 390);
                minutesToolStripMenuItem2_Click(sender, e);
            }
            else
            {
                for (int i = 0; i< 3; i++)
                {
                    Console.Beep(3333, 130);
                    Console.Beep(3333, 130);
                    Console.Beep(3333, 390);
                    Console.Beep(37, 300 - i * 100);
                }
                timer2.Stop();
                timer1.Start();
            }
        }
        
        private void Form1_Resize(object sender, EventArgs e)
        {
            while (-0.5f>((this.Height * this.Width)-areea) || ((this.Height * this.Width) - areea)> 0.5f)
            {
                if ((this.Width < label1.Width)|| (this.Height < label1.Height))
                {
                    label1.Font = new Font(label1.Font.FontFamily.Name, label1.Font.Size * 0.99f);
                }
                if (((this.Width - label1.Width)<5) || ((this.Height - label1.Height)<5))
                {
                    break;
                }else
                {
                    label1.Font = new Font(label1.Font.FontFamily.Name, label1.Font.Size * 1.01f);
                }
                areea = this.Height * this.Width;
            }
            
         }

        private void minutesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (fiftytwo != 2)
            {
                fiftytwo = 2;
                timertime = DateTime.Now.AddMinutes(52);
                timer2.Start();
            }
            else
            {
                fiftytwo = 1;
                timertime = DateTime.Now.AddMinutes(17);
                timer2.Start();
            }
        }

        private void minutesToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (fiftytwo != 4)
            {
                fiftytwo = 4;
                timertime = DateTime.Now.AddMinutes(28);
                timer2.Start();
            }
            else
            {
                fiftytwo = 3;
                timertime = DateTime.Now.AddMinutes(8);
                timer2.Start();
            }
        }

        private void minutesToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            fiftytwo = 0;
            timertime = DateTime.Now.AddMinutes(30).AddTicks(time2.Ticks);
            timer2.Start();
        }

        private void hourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fiftytwo = 0;
            timertime = DateTime.Now.AddHours(1).AddTicks(time2.Ticks);
            timer2.Start();
        }

        private void minutesToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            fiftytwo = 0;
            timertime = DateTime.Now.AddMinutes(15).AddTicks(time2.Ticks);
            timer2.Start();
        }

        private void minutesToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            fiftytwo = 0;
            timertime = DateTime.Now.AddMinutes(10).AddTicks(time2.Ticks);
            timer2.Start();
        }

        private void minutesToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            fiftytwo = 0;
            timertime = DateTime.Now.AddMinutes(5).AddTicks(time2.Ticks);
            timer2.Start();
        }

        private void windowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.FormBorderStyle = this.FormBorderStyle == FormBorderStyle.None ? FormBorderStyle.Sizable : FormBorderStyle.None;
        }

        private void stickToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.TopMost = this.TopMost ? false : true;
        }

        private void pauseplayToolStripMenuItem_Click_1(object sender, EventArgs e)
                {
                    if (!timer1.Enabled)
                    {
                        switch (timer2.Enabled)
                        {
                            case true:
                                timer2.Enabled = false;
                                timertime = new DateTime(timertime.Subtract(DateTime.Now).Ticks);
                                break;
                            case false:
                                timertime = timertime.AddTicks(DateTime.Now.Ticks);
                                timer2.Enabled = true;
                                break;

                        }
                    }
            
                }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_SizeChanged(object sender, EventArgs e)
        {
            label1.Left = (this.ClientSize.Width - label1.Size.Width) / 2;
        }
    }
}
